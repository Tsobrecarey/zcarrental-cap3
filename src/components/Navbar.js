import React, { useState, useEffect } from "react";
import {
  Navbar,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  NavbarText,
} from "reactstrap";

const adminItems = [
  {
    name: "Home",
    link: "/home",
  },
  {
    name: "Cars",
    link: "/cars",
  },
  {
    name: "Rates",
    link: "/rates",
  },
];

const userItems = [
  {
    name: "Home",
    link: "/home",
  },
  {
    name: "Bookings",
    link: "/bookings",
  },
];

const Navibar = (props) => {
  const [navItems, setNavItems] = useState([]);

  useEffect(() => {
    if (sessionStorage.isAdmin === "true") {
      setNavItems(adminItems);
    } else {
      setNavItems(userItems);
    }
  }, []);

  return (
    <Navbar
      style={{
        backgroundColor: "darkgrey",
        position: "fixed",
        width: "100%",
        height: "12vh",
      }}
    >
      <NavbarBrand href="/">
        <img
          src="/logo.png"
          className="d-inline block align-top"
          width="225"
          alt="Z-Car-Rental Logo"
        />
      </NavbarBrand>
      <Nav>
        <NavItem className="d-flex justify-content-center align-items-center">
          {navItems.map((nav, index) => (
            <NavLink key={index} href={nav.link} style={{ color: "black" }}>
              {nav.name}
            </NavLink>
          ))}
        </NavItem>
        <NavbarText style={{ color: "black" }}>
          {sessionStorage ? sessionStorage.userName : ""}
        </NavbarText>
      </Nav>
    </Navbar>
  );
};

export default Navibar;
