import React, { useEffect } from "react";
import { Link } from "react-router-dom";
import { Button } from "reactstrap";

const Home = (props) => {
  useEffect(() => {
    if (!sessionStorage.token) {
      window.location.replace("/login");
    }
  });

  const handleLogout = () => {
    sessionStorage.clear();
    window.location.replace("/login");
  };

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ width: "95%", height: "95%", marginTop: "2rem" }}
      >
        <div
          className="d-flex justify-content-center align-items-center border rounded flex-column"
          style={{
            width: "50%",
            height: "70%",
            backgroundColor: "rgba(255, 255, 255, 0.6)",
            marginTop: "2rem",
          }}
        >
          <h1 className="text-center" style={{ opacity: "1", margin: "1rem" }}>
            Hello {sessionStorage.userName}
          </h1>
          <h3 className="text-center" style={{ opacity: "1", margin: "1rem" }}>
            Start Booking for your trip today!
          </h3>
          <Link to="/bookings">
            <Button
              style={{
                width: "200px",
                margin: "1rem",
                backgroundColor: "seagreen",
              }}
            >
              Book a Car!
            </Button>
          </Link>
          <Button
            style={{
              width: "200px",
              margin: "1rem",
              backgroundColor: "firebrick",
            }}
            onClick={handleLogout}
          >
            Logout
          </Button>
        </div>
      </div>
    </div>
  );
};

export default Home;
