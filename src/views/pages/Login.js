import React, { useEffect, useState } from "react";
import { FormGroup, Label, Input, Button } from "reactstrap";
import { Link } from "react-router-dom";

const Login = (props) => {
  useEffect(() => {
    sessionStorage.clear();
  }, []);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const handleLogin = async () => {
    const apiOptions = {
      method: "POST",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify({
        email,
        password,
      }),
    };

    const fetchedData = await fetch(
      "https://dry-castle-98060.herokuapp.com/login",
      apiOptions
    );
    const data = await fetchedData.json();

    sessionStorage.token = data.token;
    sessionStorage.userId = data.user.id;
    sessionStorage.isAdmin = data.user.isAdmin;
    sessionStorage.userName = data.user.firstName + " " + data.user.lastName;
    sessionStorage.email = data.user.email;

    window.location.replace("/home");
  };

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div
        style={{
          width: "40vw",
          backgroundColor: "rgba(255, 255, 255, 0.6)",
          padding: "2rem",
        }}
      >
        <h1 className="text-center">Login</h1>
        <form>
          <FormGroup>
            <Label>E-mail:</Label>
            <Input
              type="email"
              placeholder="E-mail"
              onChange={(e) => setEmail(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Password</Label>
            <Input
              type="password"
              placeholder="Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormGroup>
          <Button
            disabled={email === "" || password === "" ? true : false}
            style={{ backgroundColor: "seagreen" }}
            block
            onClick={handleLogin}
          >
            Login
          </Button>
          <Link to="/register">
            <h6 className="text-center">Not a member? Register now!</h6>
          </Link>
        </form>
      </div>
    </div>
  );
};

export default Login;
