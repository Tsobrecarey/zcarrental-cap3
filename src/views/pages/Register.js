import React, { useState } from "react";
import { FormGroup, Label, Input, Button } from "reactstrap";

const Register = (props) => {
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [confirmPassword, setConfirmPassword] = useState("");

  const handleRegister = async () => {
    const apiOptions = {
      method: "POST",
      headers: { "Content-type": "application/json" },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        password,
      }),
    };

    await fetch("https://dry-castle-98060.herokuapp.com/register", apiOptions);

    window.location.replace("/login");
  };

  return (
    <div className="d-flex justify-content-center align-items-center vh-100">
      <div
        style={{
          width: "40vw",
          backgroundColor: "rgba(255, 255, 255, 0.6)",
          padding: "2rem",
        }}
      >
        <h1 className="text-center">Register</h1>
        <form>
          <FormGroup>
            <Label>First Name:</Label>
            <Input
              type="text"
              placeholder="First Name"
              onChange={(e) => setFirstName(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Last Name:</Label>
            <Input
              type="text"
              placeholder="Enter your last name"
              onChange={(e) => setLastName(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Email:</Label>
            <Input
              type="email"
              placeholder="Enter your Email"
              onChange={(e) => setEmail(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Password:</Label>
            <Input
              type="password"
              placeholder="Enter your Password"
              onChange={(e) => setPassword(e.target.value)}
            />
          </FormGroup>
          <FormGroup>
            <Label>Confirm Password:</Label>
            <Input
              type="password"
              placeholder="Confirm Password"
              onChange={(e) => setConfirmPassword(e.target.value)}
            />
            <span className="text-danger">
              {confirmPassword !== "" && confirmPassword !== password
                ? "Passwords did not match"
                : ""}
            </span>
          </FormGroup>
          <Button
            type="button"
            style={{ backgroundColor: "seagreen" }}
            block
            disabled={
              firstName === "" ||
              lastName === "" ||
              email === "" ||
              password === "" ||
              password !== confirmPassword
                ? true
                : false
            }
            onClick={handleRegister}
          >
            Register
          </Button>
        </form>
      </div>
    </div>
  );
};

export default Register;
