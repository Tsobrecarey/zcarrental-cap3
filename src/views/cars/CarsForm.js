import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Button,
} from "reactstrap";

const CarsForm = (props) => {
  const [rate, setRate] = useState([]);
  const [isOpenRates, setIsOpenRates] = useState(false);

  const [make, setMake] = useState("");
  const [model, setModel] = useState("");
  const [image, setImage] = useState(null);
  const [year, setYear] = useState("");
  const [capacity, setCapacity] = useState("");
  const [gas, setGas] = useState("");
  const [rates, setRates] = useState("");

  useEffect(() => {
    fetch("https://dry-castle-98060.herokuapp.com/admin/rates")
      .then((res) => res.json())
      .then((res) => {
        setRate(res);
      });
  });

  const saveCar = async () => {
    let mainImage = await uploadMainImage(image);
    props.handleSaveCar(
      make,
      model,
      mainImage.imageUrl,
      year,
      capacity,
      gas,
      rates
    );
    setMake("");
    setModel("");
    setImage("");
    setYear("");
    setCapacity("");
    setGas("");
    setRates("");
  };

  const selectMainImage = (e) => {
    let image = e.target.files[0];
    if (image.name.match(/\.(jpg| JPG | JPEG | jpeg | png | gif)$/)) {
      setImage(image);
    }
  };

  const uploadMainImage = async (imageToSave) => {
    const data = new FormData();
    data.append("image", image, imageToSave.name);

    const imageData = await fetch(
      "https://dry-castle-98060.herokuapp.com/upload",
      {
        method: "POST",
        body: data,
      }
    );
    const imageUrl = await imageData.json();

    return imageUrl;
  };

  return (
    <Modal isOpen={props.showForm} toggle={props.toggleForm}>
      <ModalHeader toggle={props.toggleForm}>
        {props.isEditing ? "Edit Car" : "Add Car"}
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label>Make:</Label>
          <Input
            placeholder="Make"
            onChange={(e) => setMake(e.target.value)}
            defaultValue={props.isEditing ? props.carsToEdit.make : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Model:</Label>
          <Input
            placeholder="Model"
            onChange={(e) => setModel(e.target.value)}
            defaultValue={props.isEditing ? props.carsToEdit.model : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Image:</Label>
          <Input
            placeholder="Image"
            type="file"
            onChange={selectMainImage}
            defaultValue={props.isEditing ? props.carsToEdit.image : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Year:</Label>
          <Input
            type="number"
            placeholder="Year"
            onChange={(e) => setYear(e.target.value)}
            defaultValue={props.isEditing ? props.carsToEdit.year : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Capacity:</Label>
          <Input
            type="number"
            placeholder="Capacity"
            onChange={(e) => setCapacity(e.target.value)}
            defaultValue={props.isEditing ? props.carsToEdit.capacity : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Gas</Label>
          <Input
            type="number"
            placeholder="km/l"
            onChange={(e) => setGas(e.target.value)}
            defaultValue={props.isEditing ? props.carsToEdit.gas : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Rate</Label>
          <Dropdown
            isOpen={isOpenRates}
            toggle={() => setIsOpenRates(!isOpenRates)}
          >
            <DropdownToggle caret>
              {!rates ? "Choose Rate" : rates.name}
            </DropdownToggle>
            <DropdownMenu>
              {rate.map((indivRate) => (
                <DropdownItem
                  key={indivRate._id}
                  onClick={() => setRates(indivRate)}
                >
                  {indivRate.name}
                </DropdownItem>
              ))}
            </DropdownMenu>
          </Dropdown>
        </FormGroup>
        <Button style={{ backgroundColor: "seagreen" }} block onClick={saveCar}>
          {props.isEditing ? "Edit Car" : "Add Car"}
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default CarsForm;
