import React from "react";
import {
  Card,
  CardHeader,
  CardImg,
  CardBody,
  CardFooter,
  Button,
} from "reactstrap";

const CarsRow = ({ car, editCar, handleDeleteCar }) => {
  return (
    <Card
      className="border col-lg-4"
      style={{
        backgroundColor: "rgba(255, 255, 255, 0.6)",
        margin: "1rem",
      }}
    >
      <CardHeader>
        <CardImg top width="100%" src={car.image} alt="Preview of Car" />
        <h3>
          {car.make} {car.model}
        </h3>
      </CardHeader>
      <CardBody>
        <h5>Year: {car.year}</h5>
        <h5>Capacity: {car.capacity} people</h5>
        <h5>Gas: {car.gas} km/l</h5>
        <h5>Rate: Php {car.rate.amount}/km</h5>
      </CardBody>
      <CardFooter className="d-flex justify-content-center align-items-center">
        <Button
          className="mx-1"
          style={{ width: "100px", backgroundColor: "cornflowerblue" }}
          onClick={() => editCar(car)}
        >
          Edit
        </Button>
        <Button
          className="mx-1"
          style={{ width: "100px", backgroundColor: "firebrick" }}
          onClick={() => handleDeleteCar(car._id)}
        >
          Delete
        </Button>
      </CardFooter>
    </Card>
  );
};

export default CarsRow;
