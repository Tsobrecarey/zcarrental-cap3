import React, { useState, useEffect } from "react";
import CarsRow from "./CarsRow";
import CarsForm from "./CarsForm";
import { Button } from "reactstrap";
import Loader from "../../components/Loader";

const Cars = (props) => {
  const [cars, setCars] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [carsToEdit, setCarsToEdit] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!sessionStorage.token) {
      window.location.replace("/login");
    }
  });

  useEffect(() => {
    setIsLoading(true);
    fetch("https://dry-castle-98060.herokuapp.com/admin/cars")
      .then((res) => res.json())
      .then((res) => {
        setCars(res);
        setIsLoading(false);
      });
  }, []);

  const toggleForm = () => {
    setShowForm(false);
  };

  const handleSaveCar = (make, model, image, year, capacity, gas, rate) => {
    setIsLoading(true);

    if (isEditing) {
      let editingMake = make;
      let editingModel = model;
      let editingImage = image;
      let editingYear = year;
      let editingCapacity = capacity;
      let editingGas = gas;
      let editingRate = rate;

      if (make === "") editingMake = carsToEdit.make;
      if (model === "") editingModel = carsToEdit.model;
      if (image === "") editingImage = carsToEdit.image;
      if (year === "") editingYear = carsToEdit.year;
      if (capacity === "") editingCapacity = carsToEdit.capacity;
      if (gas === "") editingGas = carsToEdit.gas;
      if (rate === "") editingRate = carsToEdit.rate;

      const apiOptions = {
        method: "PATCH",
        headers: { "Content-Type": "application.json" },
        body: JSON.stringify({
          id: carsToEdit._id,
          make: editingMake,
          model: editingModel,
          image: editingImage,
          year: editingYear,
          capacity: editingCapacity,
          gas: editingGas,
          rate: editingRate,
        }),
      };

      fetch(
        "https://dry-castle-98060.herokuapp.com/admin/updatecar",
        apiOptions
      )
        .then((res) => res.json())
        .then((res) => {
          let newCar = cars.map((car) => {
            if (car._id === carsToEdit._id) {
              return res;
            }
            return car;
          });
          setIsLoading(false);
          setCars(newCar);
        });
    } else {
      let apiOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          make,
          model,
          image,
          year,
          capacity,
          gas,
          rate,
          token: sessionStorage.token,
        }),
      };

      fetch("https://dry-castle-98060.herokuapp.com/admin/addcar", apiOptions)
        .then((res) => res.json())
        .then((res) => {
          res.rate = rate;
          const newCar = [res, ...cars];
          setCars(newCar);
          setIsLoading(false);
        });
    }
    setShowForm(false);
    setCarsToEdit({});
    setIsEditing(false);
  };

  const editCar = (car) => {
    setShowForm(true);
    setIsEditing(true);
    setCarsToEdit(car);
  };

  const handleDeleteCar = (id) => {
    let apiOptions = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ id, token: sessionStorage.token }),
    };

    fetch("https://dry-castle-98060.herokuapp.com/admin/deletecar", apiOptions)
      .then((res) => res.json())
      .then((res) => {
        const newCar = cars.filter((car) => {
          return car._id !== id;
        });
        setCars(newCar);
      });
  };

  if (isLoading) return <Loader />;
  return (
    <div className="d-flex justify-content-center align-items-center flex-column vh-100">
      <div className="d-flex justify-content-center align-items-center flex-column my-3">
        <h1>Cars</h1>
        <Button
          style={{
            width: "100px",
            margin: "0 1rem",
            backgroundColor: "seagreen",
          }}
          onClick={() => setShowForm(!showForm)}
        >
          Add Car
        </Button>
        <CarsForm
          showForm={showForm}
          toggleForm={toggleForm}
          handleSaveCar={handleSaveCar}
          carsToEdit={carsToEdit}
          isEditing={isEditing}
        />
      </div>
      <div className="d-flex justify-content-center align-items-center">
        {cars.map((car) => (
          <CarsRow
            key={car._id}
            car={car}
            editCar={editCar}
            handleDeleteCar={handleDeleteCar}
          />
        ))}
      </div>
    </div>
  );
};

export default Cars;
