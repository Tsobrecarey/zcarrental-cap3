import React, { useState, useEffect } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  Label,
  Dropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  FormGroup,
  Input,
  ModalFooter,
  Button,
  Col,
} from "reactstrap";
import { TimePicker } from "antd";
import "antd/lib/time-picker/style/css";
import DayPicker, { DateUtils } from "react-day-picker";
import "react-day-picker/lib/style.css";
import { Helmet } from "react-helmet";

const BookingForm = ({
  showForm,
  setShowForm,
  isEditing,
  bookingToEdit,
  handleSaveCar,
  ...props
}) => {
  BookingForm.defaultProps = {
    numberOfMonths: 2,
  };

  const [dateRange, setDateRange] = useState({
    start: undefined,
    to: undefined,
  });
  const [cars, setCars] = useState([]);
  const [isOpenCars, setIsOpenCars] = useState(false);

  const [car, setCar] = useState("");
  const [locationFrom, setLocationFrom] = useState("");
  const [locationTo, setLocationTo] = useState("");
  const [pickUpTime, setPickUpTime] = useState("");
  const [isTwoWay, setIsTwoWay] = useState("");
  const [isLodge, setIsLodge] = useState("");
  const [isMeals, setIsMeals] = useState("");
  const [amount, setAmount] = useState("");
  const [blockedDates, setBlockedDates] = useState([]);

  useEffect(() => {
    fetch("https://dry-castle-98060.herokuapp.com/admin/cars")
      .then((res) => res.json())
      .then((res) => {
        setCars(res);
      });
  }, []);

  const handleChooseCar = (indivCar) => {
    setCar(indivCar);
    const dates = [];
    props.bookings.forEach((indivBooking) => {
      if (indivBooking.car._id === indivCar._id) {
        dates.push({
          start: indivBooking.startDate,
          end: indivBooking.endDate,
        });
      }
    });
    setBlockedDates(dates);
  };

  const handleChooseDates = (day, modifiers = {}) => {
    if (modifiers.disabled) {
      return;
    }
    console.log(day);
    const range = DateUtils.addDayToRange(day, dateRange);
    setDateRange(range);
    console.log(range);
  };
  const { from, to } = dateRange;
  const modifiers = { start: from, end: to };

  const disabledDays = [{ before: new Date() }];

  blockedDates.forEach((indivDates) => {
    disabledDays.push({
      after: new Date(indivDates.start),
      before: new Date(indivDates.end),
    });
  });

  const handlePickUpTime = (_, timeString) => {
    setPickUpTime(timeString);
  };

  return (
    <React.Fragment>
      <Helmet>
        <style>{`
          .Selectable .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
            background-color: #f0f8ff !important;
            color: #4a90e2;
          }
          .Selectable .DayPicker-Day {
            border-radius: 0 !important;
          }
          .Selectable .DayPicker-Day--start {
            border-top-left-radius: 50% !important;
            border-bottom-left-radius: 50% !important;
          }
          .Selectable .DayPicker-Day--end {
            border-top-right-radius: 50% !important;
            border-bottom-right-radius: 50% !important;
          }
        `}</style>
      </Helmet>
      <Modal isOpen={showForm} toggle={() => setShowForm(!showForm)} size="lg">
        <ModalHeader toggle={() => setShowForm(!showForm)}>
          {isEditing ? "Edit Booking" : "Rent A Car"}
        </ModalHeader>
        <ModalBody>
          <FormGroup>
            <Label>Car:</Label>
            <Dropdown
              isOpen={isOpenCars}
              toggle={() => setIsOpenCars(!isOpenCars)}
            >
              <DropdownToggle caret>
                {!car ? "Choose a car" : car.model}
              </DropdownToggle>
              <DropdownMenu>
                {cars.map((indivCar) => (
                  <DropdownItem
                    key={indivCar._id}
                    onClick={() => handleChooseCar(indivCar)}
                  >
                    {indivCar.make} {indivCar.model}
                  </DropdownItem>
                ))}
              </DropdownMenu>
            </Dropdown>
          </FormGroup>
          <FormGroup>
            <Label>Location From:</Label>
            <Input
              placeholder="Pick-Up Address"
              onChange={(e) => setLocationFrom(e.target.value)}
              defaultValue={isEditing ? bookingToEdit.locationFrom : ""}
            />
          </FormGroup>
          <FormGroup>
            <Label>Location To:</Label>
            <Input
              placeholder="Destination Address"
              onChange={(e) => setLocationTo(e.target.value)}
              defaultValue={isEditing ? bookingToEdit.locationTo : ""}
            />
          </FormGroup>
          <FormGroup>
            <Label>Choose Dates:</Label>
            <DayPicker
              numberOfMonths={props.numberOfMonths}
              onDayClick={handleChooseDates}
              className="Selectable"
              selectedDays={[from, { from, to }]}
              modifiers={modifiers}
              disabledDays={disabledDays}
            />
          </FormGroup>
          <FormGroup>
            <Label>Pick-up Time: </Label>
            <TimePicker onChange={handlePickUpTime} />
          </FormGroup>
          <FormGroup style={{ padding: "0.5rem" }}>
            <Col sm={{ size: 10 }}>
              <FormGroup>
                <Label check>
                  <Input
                    type="checkbox"
                    defaultChecked={isEditing ? bookingToEdit.isTwoWay : false}
                    onChange={(e) => setIsTwoWay(!bookingToEdit.isTwoWay)}
                  />
                  Two-Way Trip
                </Label>
              </FormGroup>
              <FormGroup>
                <Label check>
                  <Input
                    type="checkbox"
                    defaultChecked={isEditing ? bookingToEdit.isLodge : false}
                    onChange={(e) => setIsLodge(!bookingToEdit.isLodge)}
                  />
                  Driver's lodging
                </Label>
              </FormGroup>
              <FormGroup>
                <Label check>
                  <Input
                    type="checkbox"
                    defaultChecked={isEditing ? bookingToEdit.isMeals : false}
                    onChange={(e) => setIsMeals(!bookingToEdit.isLodge)}
                  />
                  Driver's Meals
                </Label>
              </FormGroup>
            </Col>
          </FormGroup>
          <FormGroup>
            <Label>Amount: {bookingToEdit.amount}</Label>
            <Input
              type="number"
              onChange={(e) => setAmount(e.target.value)}
              defaultValue={isEditing ? bookingToEdit.amount : ""}
            />
          </FormGroup>
        </ModalBody>
        <ModalFooter className="d-flex justify-content-center align-items-center">
          <Button
            style={{ backgroundColor: "seagreen" }}
            onClick={() =>
              handleSaveCar(
                car,
                locationFrom,
                locationTo,
                dateRange,
                pickUpTime,
                isTwoWay,
                isLodge,
                isMeals,
                amount
              )
            }
          >
            {isEditing ? "Edit Booking" : "Complete Booking"}
          </Button>
        </ModalFooter>
      </Modal>
    </React.Fragment>
  );
};

export default BookingForm;
