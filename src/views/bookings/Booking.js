import React, { useState, useEffect } from "react";
import BookingRow from "./BookingRow";
import BookingForm from "./BookingForm";
import { Button } from "reactstrap";
import Loader from "../../components/Loader";

const Bookings = (props) => {
  const [bookings, setBookings] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [showForm, setShowForm] = useState(false);
  const [bookingToEdit, setBookingToEdit] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!sessionStorage.token) {
      window.location.replace("/login");
    }
  });

  useEffect(() => {
    setIsLoading(true);
    fetch("https://dry-castle-98060.herokuapp.com/admin/bookings")
      .then((res) => res.json())
      .then((res) => {
        setBookings(res);
        setIsLoading(false);
      });
  }, []);

  const handleSaveCar = (
    car,
    locationFrom,
    locationTo,
    dateRange,
    pickUpTime,
    isTwoWay,
    isLodge,
    isMeals,
    amount
  ) => {
    setIsLoading(true);
    if (isEditing) {
      let editingCar = car;
      let editingLocationFrom = locationFrom;
      let editingLocationTo = locationTo;
      let editingStartDate = dateRange.start;
      let editingEndDate = dateRange.end;
      let editingPickUpTime = pickUpTime;
      let editingIsTwoWay = isTwoWay;
      let editingIsLodge = isLodge;
      let editingIsMeals = isMeals;
      let editingAmount = amount;

      if (car === "") editingCar = bookingToEdit.car;
      if (locationFrom === "") editingLocationFrom = bookingToEdit.locationFrom;
      if (locationTo === "") editingLocationTo = bookingToEdit.locationTo;
      if (editingStartDate === "") editingStartDate = bookingToEdit.startDate;
      if (editingEndDate === "") editingEndDate = bookingToEdit.endDate;
      if (pickUpTime === "") editingPickUpTime = bookingToEdit.pickUpTime;
      if (isTwoWay === "") editingIsTwoWay = bookingToEdit.isTwoWay;
      if (isLodge === "") editingIsLodge = bookingToEdit.isLodge;
      if (isMeals === "") editingIsMeals = bookingToEdit.isMeals;
      if (amount === "") editingAmount = bookingToEdit.amount;

      const apiOptions = {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id: bookingToEdit._id,
          car: editingCar,
          locationFrom: editingLocationFrom,
          locationTo: editingLocationTo,
          startDate: editingStartDate,
          endDate: editingEndDate,
          pickUpTime: editingPickUpTime,
          isTwoWay: editingIsTwoWay,
          isLodge: editingIsLodge,
          isMeals: editingIsMeals,
          amount: editingAmount,
        }),
      };

      fetch(
        "https://dry-castle-98060.herokuapp.com/admin/updatebooking",
        apiOptions
      )
        .then((res) => res.json())
        .then((res) => {
          let newBooking = bookings.map((booking) => {
            if (booking._id !== bookingToEdit._id) {
              return res;
            }
            return booking;
          });
          setIsLoading(false);
          setBookings(newBooking);
        });
    } else {
      let apiOptions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          userId: sessionStorage.userId,
          car,
          locationFrom,
          locationTo,
          startDate: dateRange.from,
          endDate: dateRange.to,
          pickUpTime,
          isTwoWay,
          isLodge,
          isMeals,
          amount,
          token: sessionStorage.token,
        }),
      };

      fetch(
        "https://dry-castle-98060.herokuapp.com/admin/addbooking",
        apiOptions
      )
        .then((res) => res.json())
        .then((res) => {
          res.car = car;
          const newBooking = [res, ...bookings];
          setBookings(newBooking);
          setIsLoading(false);
        });
    }
    setShowForm(false);
    setIsEditing(false);
    setBookingToEdit({});
  };

  const handleDeleteBooking = (id) => {
    let apiOptions = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ id, token: sessionStorage.token }),
    };

    fetch(
      "https://dry-castle-98060.herokuapp.com/admin/deletebooking",
      apiOptions
    )
      .then((res) => res.json())
      .then((res) => {
        const newBooking = bookings.filter((booking) => {
          return booking._id !== id;
        });
        setBookings(newBooking);
      });
  };

  const updateBooking = (id) => {
    const newBooking = bookings.map((indivBooking) => {
      if (indivBooking._id === id) {
        indivBooking.status = "Paid";
        indivBooking.payment = "Stripe";
      }
      return indivBooking;
    });
    setBookings(newBooking);
  };

  if (isLoading) return <Loader />;
  return (
    <div className="d-flex justify-content-center align-items-center flex-column vh-100">
      <div
        className="d-flex justify-content-center align-items-center flex-column"
        style={{ marginTop: "10rem" }}
      >
        <h1 style={{ marginBottom: "1rem" }}>Bookings</h1>
        <Button
          style={{
            width: "200px",
            margin: "0 1rem",
            backgroundColor: "seagreen",
          }}
          onClick={() => setShowForm(!showForm)}
        >
          Rent a Car
        </Button>
        <BookingForm
          showForm={showForm}
          setShowForm={setShowForm}
          handleSaveCar={handleSaveCar}
          bookingToEdit={bookingToEdit}
          isEditing={isEditing}
          bookings={bookings}
        />
      </div>
      <div
        className="d-flex justify-content-center align-items-center"
        style={{ width: "95%" }}
      >
        {bookings.map((booking) => {
          return (
            <BookingRow
              key={booking._id}
              booking={booking}
              setShowForm={setShowForm}
              setIsEditing={setIsEditing}
              setBookingToEdit={setBookingToEdit}
              updateBooking={updateBooking}
              handleDeleteBooking={handleDeleteBooking}
            />
          );
        })}
      </div>
    </div>
  );
};

export default Bookings;
