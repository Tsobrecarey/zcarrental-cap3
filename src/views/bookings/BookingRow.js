import React, { useState } from "react";
import PaymentForm from "./PaymentForm";
import { Card, CardHeader, CardBody, CardFooter, Button } from "reactstrap";
import { Elements } from "@stripe/react-stripe-js";
import { loadStripe } from "@stripe/stripe-js";
import DownloadTicket from "./DownloadTicket";

const stripePromise = loadStripe("pk_test_Wo0nzemsYjh4Wa2mu8uvzq0V00uCzNdVOh");

const BookingRow = ({
  booking,
  setShowForm,
  setIsEditing,
  setBookingToEdit,
  handleDeleteBooking,
  updateBooking,
}) => {
  const [showPaymentForm, setShowPaymentForm] = useState(false);
  return (
    <Card
      className="col-lg-4"
      style={{
        backgroundColor: "rgba(255, 255, 255, 0.6)",
        margin: "1rem",
      }}
    >
      <CardHeader>
        <h4 className="text-center">Destination</h4>
        <h4 className="text-center">
          From: {booking.locationFrom} To: {booking.locationTo}
        </h4>
      </CardHeader>
      <CardBody>
        <h5>
          Car: {booking.car.make} {booking.car.model}
        </h5>
        <h5>
          Date: {booking.startDate} - {booking.endDate}
        </h5>
        <h5>Pick-up Time: {booking.pickUpTime}</h5>
        <h5>Two-Way Trip: {booking.isTwoWay ? "Yes" : "No"}</h5>
        <h5>Driver's Meals: {booking.isMeals ? "Included" : "Not Included"}</h5>
        <h5>
          Driver's Lodging: {booking.isLodge ? "Included" : "Not Included"}
        </h5>
        <h5>Amount: {booking.amount}</h5>
        <h5>Status: {booking.status}</h5>
      </CardBody>
      <CardFooter className="text-center" style={{ padding: 0 }}>
        <Button
          className="mx-1"
          style={{ width: "100px", backgroundColor: "cornflowerblue" }}
          onClick={() => {
            setShowForm(true);
            setIsEditing(true);
            setBookingToEdit(booking);
          }}
        >
          Edit
        </Button>
        <Button
          className="mx-1"
          style={{ width: "100px", backgroundColor: "firebrick" }}
          onClick={() => handleDeleteBooking(booking._id)}
        >
          Delete
        </Button>
        <Button
          className="mx-1"
          style={{ width: "125px", backgroundColor: "seagreen" }}
          onClick={() => setShowPaymentForm(true)}
        >
          Pay Booking
        </Button>
        {booking.status === "Paid" && <DownloadTicket booking={booking} />}
        <Elements stripe={stripePromise}>
          <PaymentForm
            setShowPaymentForm={setShowPaymentForm}
            showPaymentForm={showPaymentForm}
            amount={booking.amount}
            bookingId={booking._id}
            updateBooking={updateBooking}
          />
        </Elements>
      </CardFooter>
    </Card>
  );
};

export default BookingRow;
