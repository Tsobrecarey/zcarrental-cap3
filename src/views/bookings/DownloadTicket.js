import React, { useState, useEffect } from "react";
import {
  Document,
  Page,
  View,
  Text,
  StyleSheet,
  PDFDownloadLink,
} from "@react-pdf/renderer";
import { Button } from "reactstrap";

const styles = StyleSheet.create({
  header: {
    height: "100px",
    width: "100%",
    textAlign: "center",
    backgroundColor: "darkgrey",
  },
  brandName: {
    fontSize: 40,
    fontWeight: "bold",
    textAlign: "center",
  },
  detailsContainer: {
    display: "flex",
    justifyContent: "center",
  },
  details: {
    margin: "5% 20%",
  },
  detailText: {
    color: "darkgray",
    textAlign: "center",
  },
});

const Ticket = ({ booking }) => {
  return (
    <Document>
      <Page size="A4">
        <View style={styles.header}>
          <Text style={styles.brandName}>My Booking!</Text>
        </View>
        <View style={styles.detailsContainer}>
          <View style={styles.details}>
            <Text style={styles.brandName}>Booking Details</Text>
            <Text style={styles.detailText}>
              Location From: {booking.locationFrom}
            </Text>
            <Text style={styles.detailText}>
              Location To: {booking.locationTo}
            </Text>
            <Text style={styles.detailText}>
              Car: {booking.car.make} {booking.car.model}
            </Text>
            <Text style={styles.detailText}>
              Date: {booking.startDate} - {booking.endDate}
            </Text>
            <Text style={styles.detailText}>Time: {booking.pickUpTime}</Text>
            <Text style={styles.detailText}>Status: {booking.status}</Text>
            <Text style={styles.detailText}>
              Two-way Trip: {booking.isTwoWay ? "Yes" : "No"}
            </Text>
            <Text style={styles.detailText}>
              Driver Meals: {booking.isMeals ? "Included" : "Not Included"}
            </Text>
            <Text style={styles.detailText}>
              Driver Lodging: {booking.isLodge ? "Included" : "Not Included"}
            </Text>
            <Text style={styles.detailText}>
              Total Amount: {booking.amount}
            </Text>
          </View>
          <View style={styles.details}>
            <Text style={styles.detailText}>Present this upon meet-up!</Text>
          </View>
        </View>
      </Page>
    </Document>
  );
};

const DownloadTicket = ({ booking }) => {
  const [open, setOpen] = useState(false);

  useEffect(() => {
    setOpen(false);
    setOpen(true);
    return () => setOpen(false);
  });

  return (
    <Button>
      {open && (
        <PDFDownloadLink
          document={<Ticket booking={booking} />}
          fileName={"Ticket#" + Date.now() + ".pdf"}
        >
          {({ loading }) => (loading ? "Loading..." : "Download Ticket")}
        </PDFDownloadLink>
      )}
    </Button>
  );
};

export default DownloadTicket;
