import React from "react";
import { Button } from "reactstrap";

const RateRow = ({
  rate,
  setShowForm,
  setIsEditing,
  setRateToEdit,
  handleDeleteRate,
}) => {
  return (
    <tr>
      <td className="text-center">{rate.name}</td>
      <td className="text-center">{rate.amount}</td>
      <td className="text-center">
        <Button
          style={{
            width: "100px",
            margin: "0 1rem",
            backgroundColor: "cornflowerblue",
          }}
          onClick={() => {
            setShowForm(true);
            setIsEditing(true);
            setRateToEdit(rate);
          }}
        >
          Edit
        </Button>
        <Button
          style={{
            width: "100px",
            margin: "0 1rem",
            backgroundColor: "firebrick",
          }}
          onClick={() => handleDeleteRate(rate._id)}
        >
          Delete
        </Button>
      </td>
    </tr>
  );
};

export default RateRow;
