import React, { useState } from "react";
import {
  Modal,
  ModalHeader,
  ModalBody,
  FormGroup,
  Label,
  Input,
  Button,
} from "reactstrap";

const RateForm = ({
  showForm,
  setShowForm,
  handleSaveRate,
  rateToEdit,
  isEditing,
}) => {
  const [name, setName] = useState("");
  const [amount, setAmount] = useState("");
  return (
    <Modal isOpen={showForm} toggle={() => setShowForm(!showForm)}>
      <ModalHeader toggle={() => setShowForm(!showForm)}>
        {isEditing ? "Edit Rate" : "Add Rate"}
      </ModalHeader>
      <ModalBody>
        <FormGroup>
          <Label>Name:</Label>
          <Input
            placeholder="Name"
            onChange={(e) => setName(e.target.value)}
            defaultValue={isEditing ? rateToEdit.name : ""}
          />
        </FormGroup>
        <FormGroup>
          <Label>Amount:</Label>
          <Input
            placeholder="Amount"
            onChange={(e) => setAmount(e.target.value)}
            defaultValue={isEditing ? rateToEdit.amount : ""}
          />
        </FormGroup>
        <Button
          style={{ backgroundColor: "seagreen" }}
          block
          onClick={() => {
            handleSaveRate(name, amount);
            setName("");
            setAmount("");
          }}
        >
          {isEditing ? "Update Rate" : "Add Rate"}
        </Button>
      </ModalBody>
    </Modal>
  );
};

export default RateForm;
