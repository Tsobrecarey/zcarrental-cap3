import React, { useState, useEffect } from "react";
import { Table, Button } from "reactstrap";
import Loader from "../../components/Loader";
import RateRow from "./RateRow";
import RateForm from "./RateForm";

const Rates = (props) => {
  const [rates, setRates] = useState([]);
  const [isEditing, setIsEditing] = useState(false);
  const [rateToEdit, setRateToEdit] = useState({});
  const [showForm, setShowForm] = useState(false);
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    if (!sessionStorage.token) {
      window.location.replace("/login");
    }
  });

  useEffect(() => {
    setIsLoading(true);
    fetch("https://dry-castle-98060.herokuapp.com/admin/rates")
      .then((res) => res.json())
      .then((res) => {
        setRates(res);
        setIsLoading(false);
      });
  }, []);

  const handleSaveRate = (name, amount) => {
    setIsLoading(true);

    if (isEditing) {
      let editingName = name;
      let editingAmount = amount;

      if (name === "") editingName = rateToEdit.name;
      if (amount === "") editingAmount = rateToEdit.amount;

      const apiOtions = {
        method: "PATCH",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          id: rateToEdit._id,
          name: editingName,
          amount: editingAmount,
        }),
      };

      fetch(
        "https://dry-castle-98060.herokuapp.com/admin/updaterate",
        apiOtions
      )
        .then((res) => res.json())
        .then((res) => {
          let newRates = rates.map((rate) => {
            if (rate._id === rateToEdit._id) {
              return res;
            }
            return rate;
          });
          setIsLoading(false);
          setRates(newRates);
        });
    } else {
      let apiOtions = {
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          name,
          amount,
          token: sessionStorage.token,
        }),
      };

      fetch("https://dry-castle-98060.herokuapp.com/admin/addrate", apiOtions)
        .then((res) => res.json())
        .then((res) => {
          setIsLoading(false);
          setRates([...rates, res]);
        });
    }
    setShowForm(false);
    setRateToEdit({});
    setIsEditing(false);
  };

  const handleDeleteRate = (id) => {
    let apiOtions = {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({
        id,
        token: sessionStorage.token,
      }),
    };
    fetch("https://dry-castle-98060.herokuapp.com/admin/deleterate", apiOtions)
      .then((res) => res.json())
      .then((res) => {
        const newRates = rates.filter((rate) => {
          return rate._id !== id;
        });
        setRates(newRates);
      });
  };

  if (isLoading) return <Loader />;
  return (
    <div className="d-flex justify-content-center align-items-center flex-column vh-100">
      <div
        className="d-flex justify-content-center align-items-center"
        style={{
          marginTop: "2rem",
        }}
      >
        <h1>Rates</h1>
      </div>
      <Table
        className="border"
        bordered
        style={{ width: "50%", backgroundColor: "rgba(255, 255, 255, 0.6)" }}
      >
        <thead>
          <tr>
            <td className="text-center" style={{ width: "25%" }}>
              Name
            </td>
            <td className="text-center" style={{ width: "25%" }}>
              Amount
            </td>
            <td className="text-center" style={{ width: "50%" }}>
              <Button
                style={{
                  width: "230px",
                  margin: "0 1rem",
                  backgroundColor: "seagreen",
                }}
                onClick={() => setShowForm(!showForm)}
              >
                Add Rate
              </Button>
              <RateForm
                showForm={showForm}
                setShowForm={setShowForm}
                handleSaveRate={handleSaveRate}
                rateToEdit={rateToEdit}
                isEditing={isEditing}
              />
            </td>
          </tr>
        </thead>
        <tbody>
          {rates.map((rate) => (
            <RateRow
              key={rate._id}
              rate={rate}
              setShowForm={setShowForm}
              setIsEditing={setIsEditing}
              setRateToEdit={setRateToEdit}
              handleDeleteRate={handleDeleteRate}
            />
          ))}
        </tbody>
      </Table>
    </div>
  );
};

export default Rates;
