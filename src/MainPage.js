import React from "react";
import Navibar from "./components/Navbar";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import Loader from "./components/Loader";

const Home = React.lazy(() => import("./views/pages/Home"));
const Rate = React.lazy(() => import("./views/rates/Rates"));
const Car = React.lazy(() => import("./views/cars/Cars"));
const Booking = React.lazy(() => import("./views/bookings/Booking"));
const Login = React.lazy(() => import("./views/pages/Login"));
const Register = React.lazy(() => import("./views/pages/Register"));

const MainPage = () => {
  return (
    <BrowserRouter>
      <Navibar />
      <React.Suspense fallback={Loader}>
        <Switch>
          <Route path="/home" render={(props) => <Home {...props} />} />
          <Route path="/login" render={(props) => <Login {...props} />} />
          <Route path="/register" render={(props) => <Register {...props} />} />
          <Route path="/rates" render={(props) => <Rate {...props} />} />
          <Route path="/cars" render={(props) => <Car {...props} />} />
          <Route path="/bookings" render={(props) => <Booking {...props} />} />
          <Route path="/" render={(props) => <Home {...props} />} />
        </Switch>
      </React.Suspense>
    </BrowserRouter>
  );
};

export default MainPage;
